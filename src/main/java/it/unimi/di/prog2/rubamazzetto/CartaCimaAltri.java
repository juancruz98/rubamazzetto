package it.unimi.di.prog2.rubamazzetto;

import ca.mcgill.cs.stg.solitaire.cards.Card;

import java.util.Iterator;
import java.util.List;

import static it.unimi.di.prog2.rubamazzetto.Partita.TAVOLO;

public class CartaCimaAltri implements SelettoreCarta {
    private SelettoreCarta next;
    private Iterator<Giocatore> altri;

    public CartaCimaAltri(SelettoreCarta next, Iterator<Giocatore> altri) {
        this.next = next;
        this.altri = altri;
    }

    @Override
    public Card ScegliCarta(List<Card> mano) {
        for (Card card : mano) {
            for (Iterator<Giocatore> it = altri; it.hasNext(); ) {
                Giocatore giocatore = it.next();
                if (giocatore.getMazzettoTop() == card.getRank()) {
                    return card;
                }
            }
        }
        return next.ScegliCarta(mano);
    }
}