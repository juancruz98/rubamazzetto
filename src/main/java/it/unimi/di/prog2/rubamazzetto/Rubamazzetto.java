package it.unimi.di.prog2.rubamazzetto;

import java.util.ArrayList;
import java.util.List;

import static it.unimi.di.prog2.rubamazzetto.Partita.TAVOLO;

public class Rubamazzetto {
  public static void main(String[] args) {
    List<Giocatore> partecipanti = new ArrayList<>();

    Giocatore g1= new Giocatore("Antonio");
    Giocatore g2=new Giocatore("Luca");
    Giocatore g3=new Giocatore("Michela");

    partecipanti.add(g1);
    partecipanti.add(g2);
    partecipanti.add(g3);

    Partita p = new Partita(partecipanti);

    g1.setStrategia(new CartaSuTavolo(new CartaCimaAltri(new CartaRandom(), g1.getAltri())));
    g2.setStrategia(new CartaCimaMia(new CartaRandom(),g2.getMazzettoTop()));
    g3.setStrategia(new CartaRandom());

    p.distribuisciMano(3);
    while (!p.isFinita()){

      System.out.println(p);
      for (Giocatore giocatore : partecipanti) {
        giocatore.turno();
      }
      p.distribuisciMano(1);
      System.out.println(p);

    }

  }
}
