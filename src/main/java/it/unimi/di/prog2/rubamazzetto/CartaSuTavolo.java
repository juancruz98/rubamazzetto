package it.unimi.di.prog2.rubamazzetto;

import ca.mcgill.cs.stg.solitaire.cards.Card;

import java.util.List;

import static it.unimi.di.prog2.rubamazzetto.Partita.TAVOLO;

public class CartaSuTavolo implements SelettoreCarta {
    private SelettoreCarta next;

    public CartaSuTavolo(SelettoreCarta next) {
        this.next = next;
    }

    @Override
    public Card ScegliCarta(List<Card> mano) {
        for (Card card : mano) {
            if (TAVOLO.inMostra(card)){
                return card;
            }
        }
        return next.ScegliCarta(mano);
    }
}
