package it.unimi.di.prog2.rubamazzetto;

import ca.mcgill.cs.stg.solitaire.cards.Card;
import ca.mcgill.cs.stg.solitaire.cards.Rank;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static it.unimi.di.prog2.rubamazzetto.Partita.TAVOLO;

public class Giocatore {

  private final String nome;
  private List<Card> mano = new ArrayList<>();
  private Rank mazzettoTop;
  private List<Giocatore> altri = new ArrayList<>();
  private int punti;
  private SelettoreCarta strategia;

  public Giocatore(String nome) {
    this.nome = nome;
  }

  public Rank getMazzettoTop() {
    return mazzettoTop;
  }

  public Iterator<Giocatore> getAltri() {
    return altri.iterator();
  }

  public void addAltro(Giocatore g) {
    if (!altri.contains(g))
      altri.add(g);
  }

  public int getPunti() {
    return punti;
  }

  public void daiCarta(Card carta) {
    mano.add(carta);
  }

  public void setStrategia(SelettoreCarta strategia) {
    this.strategia = strategia;
  }


  public void turno() {
    assert strategia != null;
    final Card aCard = strategia.ScegliCarta(mano);

    mano.remove(aCard);

    if(TAVOLO.inMostra(aCard)){
      TAVOLO.prendi(aCard);
      punti=punti+2;
      mazzettoTop=aCard.getRank();
      return;
    }
    for (Giocatore giocatore : altri) {
      if (giocatore.getMazzettoTop()==aCard.getRank()){
        punti+=giocatore.getPunti()+1;
        giocatore.punti=0;
        mazzettoTop=aCard.getRank();
        return;
      }
    }

    TAVOLO.metti(aCard); //Se la carta avesse rank uguale alla getMazzettoTop() in ogni caso la metterei nel tavolo

  }



  @Override
  public String toString() {
    StringBuilder s = new StringBuilder(nome);
    s.append(": ");
    s.append("Carte in mano: ").append(mano.size() + ", ");
    s.append("[");
    for (Card card : mano) {
      s.append(card.toString());
      s.append(", ");
    }
    if (mano.size()>0) {
      s.replace(s.length() - 2, s.length(), "");
    }
    s.append("], ");

    if (punti > 0) {
      s.append("mazzetto con ");
      s.append(punti);
      s.append(" carte, rank cima: ");
      s.append(mazzettoTop);
      s.append("; ");
    }
    return s.toString();
  }

  public int numCards() {
    return mano.size();
  }
}
