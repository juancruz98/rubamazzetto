package it.unimi.di.prog2.rubamazzetto;

import ca.mcgill.cs.stg.solitaire.cards.Card;
import ca.mcgill.cs.stg.solitaire.cards.Rank;

import java.util.List;

public class CartaCimaMia implements SelettoreCarta {
    private SelettoreCarta next;
    private Rank rankTop;

    public CartaCimaMia(SelettoreCarta next, Rank rankTop) {
        this.next = next;
        this.rankTop = rankTop;
    }

    @Override
    public Card ScegliCarta(List<Card> mano) {
        for (Card card : mano) {
            if (card.getRank()==rankTop){
                return card;
            }
        }
        return next.ScegliCarta(mano);
    }
}
