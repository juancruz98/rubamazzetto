package it.unimi.di.prog2.rubamazzetto;

import ca.mcgill.cs.stg.solitaire.cards.Card;

import java.util.List;
import java.util.Random;

public class CartaRandom implements  SelettoreCarta {
    public CartaRandom() {
    }

    @Override
    public Card ScegliCarta(List<Card> mano) {
        Random rand = new Random();
        Card carta = mano.get(rand.nextInt(mano.size()));
        return carta;
    }
}
